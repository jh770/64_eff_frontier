cc=g++
CPPFLAGS= -std=gnu++98 -pedantic -Wall -Werror -ggdb3
efficient_frontier: main.o pharse.o
	g++ -o efficient_frontier $(CCFLAGS) main.o pharse.o

.PHONY: clean depend

depend:
	makedepend main.o pharse.o

clean:
	rm -f *.o  *~ efficient_frontier


# DO NOT DELETE

main.o: main.cpp
pharse.o: pharse.cpp asset.hpp
