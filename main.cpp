#include "asset.hpp"
//#include "pharse.cpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>
#include <iomanip>
#include <unistd.h>
using namespace Eigen;

void splitString(std::string& s, std::vector<std::string>& v, std::string& g);
double calculateSigma(Portfolio p);
double UnrestrictOptimal(Portfolio &P,double &ret);
double RestrictOptimal(Portfolio &P,double &ret);

int main(int argc, char ** argv){
  if (argc != 3 && argc !=4) {
    fprintf(stderr, "Invalid input\n");
    return EXIT_FAILURE;
  }
  if(argc ==4 && strlen(argv[1]) != 2){
      std::cerr<<"Wrong Option";//Cannot allow something like -rabcdefghijk
      return EXIT_FAILURE;
  }
//(1) check whether "-r" is passed
  int opt;
  bool r = false;
  while ((opt = getopt(argc, argv, "r")) != -1){
    switch(opt){
      case 'r':
        r = true;
        break;
      case '?':
        std::cerr<<"Option is wrong";
        return EXIT_FAILURE;
    }
  }

  //int num = 0;//the number of assets
  std::vector<Asset> A;
  char * asset_name;
  char * corr_name;
  if(r == true){
      asset_name=argv[2];
      corr_name=argv[3];
    //A = readUniverse(argv[2],num);
    //Corr = readCorrMatrix(argv[3],num);
  }
  else{
      asset_name=argv[1];
      corr_name=argv[2];
    //A = readUniverse(argv[1],num);
    //Corr = readCorrMatrix(argv[2],num);
  }
//(3) Read Universe
  std::ifstream ifs1(asset_name);
  if(!ifs1.is_open()){
    perror("open file fail");
    exit(EXIT_FAILURE);
  }
  std::string ln1;
  std::string g = ",";
  int num=0;
  while(std::getline(ifs1,ln1)){
    std::vector<std::string> t;
    splitString(ln1,t,g);
    if(t.size() != 3){
      perror("wrong format of asset");//handle non-numeric value
      exit(EXIT_FAILURE);
    }
    Asset temp;
    temp.name = t[0];
    if(atof(t[1].c_str())==0.0 || atof(t[2].c_str())==0.0){
      perror("Assets have non-numeric value");//handle non-numeric value
      exit(EXIT_FAILURE);
    }
    temp.avr = atof(t[1].c_str());
    temp.sigma = atof(t[2].c_str());
    num++;
    A.push_back(temp);
  }
  if(num == 0){
      perror("Universe file is empty");//handle empty file
      exit(EXIT_FAILURE);
    }
    std::ifstream ifs2(corr_name);
    if(!ifs2.is_open()){
      perror("open file fail");
         exit(EXIT_FAILURE);
    }

    MatrixXd Corr(num,num);
    std::string ln2;
    int i=0;
    while(std::getline(ifs2,ln2)){
      if(i == num){
        perror("Dimension of correlation matrix is bigger than the number of assets");//handle larger matrix while less assets
        exit(EXIT_FAILURE);
      }
      std::vector<std::string> t;
      splitString(ln2,t,g);
      if(t.size() != (size_t)num){
        std::cerr<<"Dimension of correlation matrix is not the same as the number of assets"<<std::endl;//handle different dimensions
        exit(EXIT_FAILURE);
      }
      for(int j=0;j<num;j++){
        if(atof(t[j].c_str())==0.0){
          std::cerr<<"Correlation matrix has non-numeric value"<<std::endl;
             exit(EXIT_FAILURE);
        }
        Corr(i,j) = atof(t[j].c_str());
      }
      i++;
    }
    if(i == 0){
      perror("Correlation file is empty");//handle empty file
      exit(EXIT_FAILURE);
    }
    for(int i=0;i<num;i++){//Actually correlation matrix shoule be positive semidefinite
      for(int j=0;j<=i;j++){
        if(fabs(Corr(i,j) - Corr(j,i)) > 0.0001 || fabs(Corr(i,j)) > 1.0001){
          perror("Correlation matrix has wrong mathematical format");//handle wrong format
          exit(EXIT_FAILURE);
        }
      }
      if(fabs(Corr(i,i) - 1) > 0.0001){
        perror("Corr(i,i) does not equal to 1");//handle Corr(i,i) != 1
        exit(EXIT_FAILURE);
      }
    }
  Portfolio P;
  P.n = num;
  P.A = A;
  P.Corr = Corr;
//(4) Step 1: Unrestricted case: return level between 1% and 26% in 1% increments.
  std::cout<<"ROR,volatility"<<std::endl;
  std::cout.setf(std::ios::fixed);
  if(r == false){
    for(double l = 0.01;l <= 0.265;l += 0.01){
      std::cout<<std::fixed<<std::setprecision(1)<<l*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<UnrestrictOptimal(P,l)*100<<"%"<<std::endl;
    }
  }
//(5) Step 2: Restricted case: return level between 1% and 26% in 1% increments.
  if(r == true){
    for(double l = 0.01;l <= 0.265;l += 0.01){
      std::cout<<std::fixed<<std::setprecision(1)<<l*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<RestrictOptimal(P,l)*100<<"%"<<std::endl;
    }
  }
  std::cout.unsetf(std::ios::fixed);
  return EXIT_SUCCESS;
}
